<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();


$container = rffw_get_field('container_type');
$sidebar_pos = rffw_get_field( 'sidebar_position');
$fullwidth_content = true;

if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos || 'left' === $sidebar_pos ) {
	$fullwidth_content = false;
}

?>

<div class="wrapper" id="page-wrapper">

	<div class="<?php echo esc_attr( $container ); ?> <?php echo $fullwidth_content ? 'fullwidth-content' : ''; ?>" id="content" tabindex="-1">

		<?php while ( have_posts() ) : the_post(); ?>

			<?php if(get_field('show_header_post')) : ?>
				<?php get_template_part( 'template-parts/header/page-header' ); ?>
			<?php endif; ?>

			<div class="row">

				<!-- Do the left sidebar check -->
				<?php get_template_part( 'template-parts/global/left-sidebar-check' ); ?>

				<main class="site-main" id="main">

					<?php get_template_part( 'template-parts/loop/content', 'page' ); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>


				</main><!-- #main -->

				<!-- Do the right sidebar check -->
				<?php get_template_part( 'template-parts/global/right-sidebar-check' ); ?>

			</div><!-- .row -->

		<?php endwhile; // end of the loop. ?>

	</div><!-- #content -->

	<?php get_template_part( 'template-parts/sidebar/footer', 'content' ); ?>

</div><!-- #page-wrapper -->

<?php get_footer(); ?>
