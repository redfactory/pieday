<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = rffw_get_field('container_type');
?>

		<?php get_template_part( 'template-parts/sidebar/footer', 'full' ); ?>

		<div class="wrapper" id="wrapper-footer">
			<div class="<?php echo esc_attr( $container ); ?>">
				<footer class="site-footer" id="colophon">
					<div class="row">
						<div class="col-6">
							<p><?php echo rffw_current_year(get_field('footer_text', 'option')); ?></p>
						</div>
						<div class="col-6">
							<?php if(rffw_has_social_links()){ ?>
								<div class="footer-social float-right">
									<?php foreach(rffw_get_social_links() as $link){ ?>
										<a href="<?php echo esc_url($link['url']); ?>"><i class="<?php esc_attr_e($link['class']); ?>"></i></a>
									<?php } ?>
								</div>
							<?php } ?>

						</div>
					</div>
				</footer>
			</div>
		</div>
	</div>

	<?php wp_footer(); ?>

	</body>
</html>
