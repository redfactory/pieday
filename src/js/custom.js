jQuery(document).ready(function($) {

  //Mobile navbar toggler
  $('.navbar-toggler').on('click', function(){
      $('#navbarNav').fadeToggle(200);
      $(this).toggleClass('open');
  });

  // Put mainmenu items in dropdown when there's too much of 'em
  var $menuMenu = $('#navbarNav').find('ul.navbar-nav');

  function hideMenuOverflow() {
    var $obj_dropdown = $('<ul class="dropdown-menu" aria-labelledby="extra-menu-items">');

    var space_available = $('.header-container').outerWidth();
    space_available = space_available - ($('.navbar-brand').outerWidth() + $('.header-social').outerWidth());
    var rffw_menu_length = space_available;

    var rffw_li_total_length = 50; //offset for extra space
    var rffw_has_dropdown = false;

    $menuMenu.find('li.more-menu').remove();

    $menuMenu.children('li').each(function(){
      $(this).removeClass('hidden');

      var rffw_li_length = $(this).outerWidth();
      rffw_li_total_length = rffw_li_total_length + rffw_li_length;

      if ( rffw_li_total_length > rffw_menu_length ) {
        rffw_has_dropdown = true;
        $obj_dropdown.append($(this).clone());
        $(this).addClass('hidden');
      }
    });

    if (rffw_has_dropdown == true) {
      var $obj_dropdown_complete = $('<li class="menu-item menu-item-has-children dropdown nav-item more-menu">').append($obj_dropdown);
      $obj_dropdown_complete.prepend('<a href="#" class="nav-link" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" id="extra-menu-items" ><i class="fas fa-chevron-down"></i></a>');
      $menuMenu.append($obj_dropdown_complete);
    }
  }

  function subMenuLeftOrRight(){
    $('ul.dropdown-menu', '#navbarNav').each(function( index ) {
      var rightPos = $(this).offset().left + $(this).outerWidth();
      var windowWidth = $( window ).width();

      if(rightPos > windowWidth){
        $(this).addClass('move');
      }else{
        $(this).removeClass('move');
      }
    });
  }


	$(window).resize(function() {
		hideMenuOverflow();
		subMenuLeftOrRight();
	});

	$(window).bind("load", function() {
		hideMenuOverflow();
		subMenuLeftOrRight();
	});

});
