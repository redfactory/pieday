/**
* Hello World: Step 4
*
* Adding extra controls: built-in alignment toolbar.
*/
( function( wp, blocks, editor, i18n, element ) {
	var el = element.createElement;
	var __ = i18n.__;
	var RichText = editor.RichText;
	var InspectorControls = editor.InspectorControls;
	var withSelect = wp.data.withSelect;
	var AlignmentToolbar = editor.AlignmentToolbar;
	var BlockControls = editor.BlockControls;
	var BlockAlignmentToolbar = editor.BlockAlignmentToolbar;
	var MediaUpload = editor.MediaUpload;
	var InnerBlocks = editor.InnerBlocks;
	var Button = wp.components.Button;
	var withFallbackStyles = wp.components.withFallbackStyles;
	var IconButton = wp.components.IconButton;
	var Dashicon = wp.components.Dashicon;
	var withState = wp.components.withState;
	var Toolbar = wp.components.Toolbar;

	//TODO: kan dit weg?
	var PanelBody = wp.components.PanelBody;
	var ColorPalette = wp.components.ColorPalette;

	//const allowedBlocks = [ 'core/paragraph' ];

	blocks.registerBlockType( 'pieday/container', {
		title: __( 'Container', 'pieday' ),
		icon: {
			src: 'editor-table',
			foreground: '#200597'
		},
		category: 'pieday-category',

		attributes: {
			alignment: {
				type: 'string',
				default: 'none',
			},
			container_width: {
				type: 'string',
			},
		},


		edit: function( props ) {
			//var content = props.attributes.content;
			//var alignment = props.attributes.alignment;
			var container_width = props.attributes.container_width;
			var background_color = props.attributes.background_color;
			var block_style = props.attributes.block_style // To bind the style of the button
			block_style = {
				backgroundColor: background_color,
				//textAlign: alignment,
			}

			function onChangeBgColor ( content ) {
				props.setAttributes({background_color: content});
			}
/*
			function onChangeContent( newContent ) {
				props.setAttributes( { content: newContent } );
			}

			function onChangeAlignment( newAlignment ) {
				props.setAttributes( { alignment: newAlignment === undefined ? 'none' : newAlignment } );
			}
*/



			return [

				el('div', { className: props.className },
					el( InnerBlocks, {  } )
				),

				el( InspectorControls, { key: 'inspector' },
					el( PanelBody, {
						title: __( 'Background color', 'gutenblock1' ),
						className: 'custom-bg-color',
						initialOpen: true,
					}),
					el( ColorPalette, {
						type: 'string',
						label: __( 'Choose a color', 'gutenblock1' ),
						value: props.attributes.background_color,
						onChange: function( newColor ) {
							props.setAttributes( { background_color: newColor } );
						},
					})
				),
				el(BlockControls,	{ key: 'controls' },
					el(BlockAlignmentToolbar,{
						value: container_width,
						onChange: function ( newWidth ) {
							props.setAttributes( { newWidth } );
						},
						controls: ['center', 'full']
					})
				),
				/*
				el(RichText, {
					key: 'richtext',
					tagName: 'p',
					style: block_style,
					className: props.className,
					onChange: onChangeContent,
					value: content,
				}),
				*/
			];
		},

		save: function( props ) {
			var block_style = {
				backgroundColor: props.attributes.background_color
			};
			return (
				/*
				el( RichText.Content, {
					tagName: 'p',
					className: 'gutenberg-examples-align-' + props.attributes.alignment,
					value: props.attributes.content,
					style: block_style
				}),
				*/

				el('div', { className: props.className }, //Need add props.className to render saved content
        	el('div', { className: 'custom-sec-inner' },
          	el( InnerBlocks.Content, null )
          )
        )

			)
		},
	});
}(
	window.wp,
	window.wp.blocks,
	window.wp.editor,
	window.wp.i18n,
	window.wp.element
) );
