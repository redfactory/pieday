<?php

function rffw_register_container_block() {

	if ( ! function_exists( 'register_block_type' ) ) {
		// Gutenberg is not active.
		return;
	}

	wp_register_script(
		'rffw-container-block-script',
		get_template_directory_uri() . '/blocks/container/block.js',
		array( 'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-rich-text', 'wp-components'),
		'1.0'
	);

	wp_register_style(
		'rffw-container-block-editor',
		get_template_directory_uri() . '/blocks/container/editor.css',
		array( 'wp-edit-blocks' ),
		'1.0'
	);

	wp_register_style(
		'rffw-container-block-style',
		get_template_directory_uri() . '/blocks/container/style.css',
		array( ),
		'1.0'
	);

	register_block_type( 'pieday/example-container', array(
		'style' => 'rffw-container-block-style',
		'editor_style' => 'rffw-container-block-editor',
		'editor_script' => 'rffw-container-block-script',
		'attributes' => array(
				'className' => array(
					'type' => 'string',
				),
				'background_color' => array(
					'type' => 'string',
				),
			),
	));

}

add_action( 'init', 'rffw_register_container_block' );
