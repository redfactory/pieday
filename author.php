<?php
/**
 * The template for displaying the author pages.
 *
 * Learn more: https://codex.wordpress.org/Author_Templates
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = rffw_get_field('container_type');
?>

<div class="wrapper" id="author-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<div class="row">

			<?php
			if ( isset( $_GET['author_name'] ) ) {
				$curauth = get_user_by( 'slug', $author_name );
			} else {
				$curauth = get_userdata( intval( $author ) );
			}
			?>

			<?php get_template_part( 'template-parts/header/author-header' ); ?>

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'template-parts/global/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<!-- <h2><?php //echo esc_html( 'Posts by', 'pieday' ) . ' ' . esc_html( $curauth->nickname ); ?>:</h2> -->

					<!-- The Loop -->
					<?php if ( have_posts() ) : ?>
						<?php while ( have_posts() ) : the_post(); ?>

							<?php

							/*
							 * Include the Post-Format-specific template for the content.
							 * If you want to override this in a child theme, then include a file
							 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
							 */
							get_template_part( 'template-parts/loop/content', get_post_format() );
							?>
						<?php endwhile; ?>

					<?php else : ?>

						<?php get_template_part( 'template-parts/loop/content', 'none' ); ?>

					<?php endif; ?>

					<!-- End Loop -->

			</main><!-- #main -->

			<!-- The pagination component -->
			<?php pieday_pagination(); ?>

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'template-parts/global/right-sidebar-check' ); ?>

		</div> <!-- .row -->

	</div><!-- #content -->

</div><!-- #author-wrapper -->

<?php get_footer(); ?>
