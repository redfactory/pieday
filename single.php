<?php
/**
 * The template for displaying all single posts.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
$container = rffw_get_field('container_type');
?>

<div class="wrapper" id="single-wrapper">

	<div class="<?php echo esc_attr( $container ); ?>" id="content" tabindex="-1">

		<?php get_template_part( 'template-parts/header/page-header' ); ?>

		<div class="row">

			<!-- Do the left sidebar check -->
			<?php get_template_part( 'template-parts/global/left-sidebar-check' ); ?>

			<main class="site-main" id="main">

				<?php while ( have_posts() ) : the_post(); ?>

					<?php get_template_part( 'template-parts/loop/content', 'single' ); ?>

					<?php
					// If comments are open or we have at least one comment, load up the comment template.
					if ( comments_open() || get_comments_number() ) :
						comments_template();
					endif;
					?>

				<?php endwhile; // end of the loop. ?>

			</main><!-- #main -->

			<!-- Do the right sidebar check -->
			<?php get_template_part( 'template-parts/global/right-sidebar-check' ); ?>

		</div><!-- .row -->

	</div><!-- #content -->

	<?php get_template_part( 'template-parts/related_posts'); ?>

</div><!-- #single-wrapper -->

<?php get_footer(); ?>
