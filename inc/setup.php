<?php
/**
 * Theme basic setup.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// Set the content width based on the theme's design and stylesheet.
if ( ! isset( $content_width ) ) {
	$content_width = 640; /* pixels */
}

add_action( 'after_setup_theme', 'pieday_setup' );

if ( ! function_exists ( 'pieday_setup' ) ) {
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function pieday_setup() {





		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on pieday, use a find and replace
		 * to change 'pieday' to the name of your theme in all the template files
		 */
		load_theme_textdomain( 'pieday', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => __( 'Primary Menu', 'pieday' ),
		) );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		/*
		 * Adding Thumbnail basic support
		 */
		add_theme_support( 'post-thumbnails' );

		/*
		 * Adding support for Widget edit icons in customizer
		 */
		add_theme_support( 'customize-selective-refresh-widgets' );

		/*
		 * Enable support for Post Formats.
		 * See http://codex.wordpress.org/Post_Formats
		 */
		add_theme_support( 'post-formats', array(
			'aside',
			'image',
			'video',
			'quote',
			'link',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'pieday_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Set up the WordPress Theme logo feature.
		add_theme_support( 'custom-logo' );

		// Add support for responsive embedded content.
		add_theme_support( 'responsive-embeds' );

		//Add support for block align
		add_theme_support( 'align-wide' );

		//Change standard editor font sizes
		add_theme_support( 'editor-font-sizes', array(
			array(
				'name'      => __( 'Small', 'pieday' ),
				'shortName' => __( 'S', 'pieday' ),
				'size'      => 13,
				'slug'      => 'small'
			),
			array(
				'name'      => __( 'Normal', 'pieday' ),
				'shortName' => __( 'N', 'pieday' ),
				'size'      => '',
				'slug'      => 'normal'
			),
			array(
				'name'      => __( 'Medium', 'pieday' ),
				'shortName' => __( 'M', 'pieday' ),
				'size'      => 22,
				'slug'      => 'medium'
			),
			array(
				'name'      => __( 'Large', 'pieday' ),
				'shortName' => __( 'L', 'pieday' ),
				'size'      => 24,
				'slug'      => 'large'
			),
			array(
				'name'      => __( 'Huge', 'pieday' ),
				'shortName' => __( 'XL', 'pieday' ),
				'size'      => 52,
				'slug'      => 'huge'
			),
		));

		if( function_exists('get_field')) {
			$editor_color_palette = array(
				array(
					'name'  => __( 'Primary Color', 'pieday' ),
					'slug'  => 'primary',
					'color'	=> get_field('primary', 'option'),
				),
				array(
					'name'  => __( 'Secondary Color', 'pieday' ),
					'slug'  => 'secondary',
					'color'	=> get_field('secondary', 'option'),
					),
			);

			$base_font = get_field('base_font', 'option');
			if(isset($base_font['text_color'])){
				$editor_color_palette[] = array(
					'name'  => __( 'Base font color ' , 'pieday' ),
					'slug'  => 'base_font_color',
					'color'	=> $base_font['text_color'],
				);
			}

			$font_heading = get_field('font_heading', 'option');
			if(isset($font_heading['text_color'])){
				$editor_color_palette[] = array(
					'name'  => __( 'Heading font color ' , 'pieday' ),
					'slug'  => 'heading_font_color',
					'color'	=> $font_heading['text_color'],
				);
			}


			$color_palettes = get_field('color_palettes', 'option');
			if(isset($color_palettes) && is_array($color_palettes)){
				$i = 1;
				foreach($color_palettes as $color){
					$editor_color_palette[] = array(
						'name'  => __( 'Color Palet '.$i , 'pieday' ),
						'slug'  => 'color_palet_'.$i,
						'color'	=> $color['color'],
					);
					$i++;
				}
			}

			add_theme_support( 'editor-color-palette',  $editor_color_palette);
		}



	}
}


add_filter( 'excerpt_more', 'pieday_custom_excerpt_more' );

if ( ! function_exists( 'pieday_custom_excerpt_more' ) ) {
	/**
	 * Removes the ... from the excerpt read more link
	 *
	 * @param string $more The excerpt.
	 *
	 * @return string
	 */
	function pieday_custom_excerpt_more( $more ) {
		if ( ! is_admin() ) {
			$more = '';
		}
		return $more;
	}
}

add_filter( 'wp_trim_excerpt', 'pieday_all_excerpts_get_more_link' );

if ( ! function_exists( 'pieday_all_excerpts_get_more_link' ) ) {
	/**
	 * Adds a custom read more link to all excerpts, manually or automatically generated
	 *
	 * @param string $post_excerpt Posts's excerpt.
	 *
	 * @return string
	 */
	function pieday_all_excerpts_get_more_link( $post_excerpt ) {
		if ( ! is_admin() ) {
			$post_excerpt = $post_excerpt . ' [...]<p><a class="btn btn-secondary pieday-read-more-link" href="' . esc_url( get_permalink( get_the_ID() ) ) . '">' . __( 'Read More...',
			'pieday' ) . '</a></p>';
		}
		return $post_excerpt;
	}
}
