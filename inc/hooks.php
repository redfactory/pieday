<?php
/**
 * Custom hooks.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'pieday_site_info' ) ) {
	/**
	 * Add site info hook to WP hook library.
	 */
	function pieday_site_info() {
		do_action( 'pieday_site_info' );
	}
}

if ( ! function_exists( 'pieday_add_site_info' ) ) {
	add_action( 'pieday_site_info', 'pieday_add_site_info' );

	/**
	 * Add site info content.
	 */
	function pieday_add_site_info() {
		$the_theme = wp_get_theme();
		$site_info = '<a href="' . esc_url( __( 'https://redfactory.nl' ) ) . '">redfactory.nl</a> '.$the_theme->get( 'Version' ) ;

		echo apply_filters( 'pieday_site_info_content', $site_info ); // WPCS: XSS ok.
	}
}
