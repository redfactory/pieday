<?php
// // Define path and URL to the ACF plugin.
define( 'RFFW_ACF_PATH', get_template_directory() . '/inc/acf/' );
//define( 'MY_ACF_URL', get_stylesheet_directory_uri() . '/inc/acf/' );

// // Include the ACF plugin.
// include_once( MY_ACF_PATH . 'acf.php' );


// Include the ACF plugin.


add_action('acf/include_field_types', 'rffw_fonts_include_field_types');
function rffw_fonts_include_field_types(){
	include_once( RFFW_ACF_PATH . '../acf-fields/rffw-fonts.php' );
	include_once( RFFW_ACF_PATH . '../acf-fields/rffw-rgba-color-picker.php' );
	include_once( RFFW_ACF_PATH . '../acf-fields/rffw-theme-options.php' );
}


// // Customize the url setting to fix incorrect asset URLs.
// add_filter('acf/settings/url', 'my_acf_settings_url');
// function my_acf_settings_url( $url ) {
//     return MY_ACF_URL;
// }

// (Optional) Hide the ACF admin menu item.
// add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
// function my_acf_settings_show_admin( $show_admin ) {
//     return false;
// }


add_filter('acf/settings/save_json', 'my_acf_json_save_point');
function my_acf_json_save_point( $path ) {

    // update path
    $path = get_template_directory() . '/inc/acf-json';

    // return
    return $path;
}


add_filter('acf/settings/load_json', 'my_acf_json_load_point');
function my_acf_json_load_point( $paths ) {

    // append path
    $paths[] = get_template_directory() . '/inc/acf-json';

    // return
    return $paths;

}



if( function_exists('acf_add_options_page') ) {

		acf_add_options_page(array(
			'page_title' 	=> __('Theme General Settings', 'pieday'),
			'menu_title'	=> __('Theme Settings', 'pieday'),
			'menu_slug' 	=> __('theme-settings', 'pieday'),
			'capability'	=> 'edit_posts',
			'redirect'		=> true,

		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Theme General Settings', 'pieday'),
			'menu_title'	=> __('General', 'pieday'),
			'parent_slug'	=> __('theme-settings', 'pieday'),
			'autoload' 		=> true,
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Color Settings', 'pieday'),
			'menu_title'	=> __('Colors', 'pieday'),
			'parent_slug'	=> __('theme-settings', 'pieday'),
			'autoload' 		=> true,
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Theme Header Settings', 'pieday'),
			'menu_title'	=> __('Header', 'pieday'),
			'parent_slug'	=> __('theme-settings', 'pieday'),
			'autoload' 		=> true,
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Theme Footer Settings', 'pieday'),
			'menu_title'	=> __('Footer', 'pieday'),
			'parent_slug'	=> __('theme-settings', 'pieday'),
			'autoload' 		=> true,
		));

		acf_add_options_sub_page(array(
			'page_title' 	=> __('Advanced Settings', 'pieday'),
			'menu_title'	=> __('Advanced', 'pieday'),
			'parent_slug'	=> __('theme-settings', 'pieday'),
		));
}


// remove settings page
remove_action( 'admin_menu', 'acft_add_admin_menu' );
remove_action( 'admin_init', 'acft_settings_init' );
