<?php
// Add mime types to update
function rffw_mime_types($mimes) {
    $mimes['svg'] = 'image/svg+xml';
    return $mimes;
  }
  add_filter('upload_mimes', 'rffw_mime_types');

// Social link builder with fontasome icons
function rffw_get_social_links(){
    $rffw_social = array();
    $wpseo_social = get_option( 'wpseo_social', array() );

    // Array
    // (
    //     [facebook_site] => https://www.facebook.com/redfactoryNL/
    //     [instagram_url] => https://www.instagram.com/patrickcohen/
    //     [linkedin_url] => https://www.linkedin.com/company/redfactory-nl/
    //     [myspace_url] => https://www.linkedin.com/company/redfactory-nl/
    //     [og_default_image] =>
    //     [og_default_image_id] =>
    //     [og_frontpage_title] =>
    //     [og_frontpage_desc] =>
    //     [og_frontpage_image] =>
    //     [og_frontpage_image_id] =>
    //     [opengraph] => 1
    //     [pinterest_url] => https://www.linkedin.com/company/redfactory-nl/
    //     [pinterestverify] =>
    //     [twitter] => 1
    //     [twitter_site] => redfactory
    //     [twitter_card_type] => summary_large_image
    //     [youtube_url] => https://www.linkedin.com/company/redfactory-nl/
    //     [wikipedia_url] => https://www.linkedin.com/company/redfactory-nl/
    //     [fbadminapp] =>
    // )

    $i = 0;

    foreach($wpseo_social as $key => $link ){

    if(isset($link) && !empty($link)){

      switch ($key) {
        case 'facebook_site':
            $rffw_social[$i]['class'] = 'fab fa-facebook-square';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'instagram_url':
            $rffw_social[$i]['class'] = 'fab fa-instagram';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'linkedin_url':
            $rffw_social[$i]['class'] = 'fab fa-linkedin';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'myspace_url':
            $rffw_social[$i]['class'] = 'fas fa-external-link-alt';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'pinterest_url':
            $rffw_social[$i]['class'] = 'fab fa-pinterest-square';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'twitter_site':
            $rffw_social[$i]['class'] = 'fab fa-twitter';
            $rffw_social[$i]['url'] = 'https://twitter.com/'.$link;
            break;
        case 'youtube_url':
            $rffw_social[$i]['class'] = 'fab fa-youtube';
            $rffw_social[$i]['url'] = $link;
            break;
        case 'wikipedia_url':
            $rffw_social[$i]['class'] = 'fab fa-wikipedia-w';
            $rffw_social[$i]['url'] = $link;
            break;
      }

      $i++;
    }
  }
  return $rffw_social;
}

// Check if social links are set
function rffw_has_social_links(){
  if(!empty(rffw_get_social_links()))
    return true;
  return false;
}

// Replace {{current_year}} with the current year
function rffw_current_year($string){
  $string = str_replace("{{current_year}}", date('Y'), $string);
  return $string;

}

function rffw_acf_custom($hook) {
    // See WP docs.
    if ('admin.php' !== $hook) {
        return;
    }
		// scripts
		wp_enqueue_script('rffw-acf-custom', get_template_directory_uri() . '/inc/js/backend-theme-options.js', array('jquery'), '', true);
		
	
}

add_action('admin_enqueue_scripts', 'rffw_acf_custom');


function rffw_is_index() {
  return ( is_archive() || is_author() || is_category() || is_home() || is_tag() || is_search());
}

function rffw_get_related_posts( $post_id, $related_count, $args = array() ) {
  $args = wp_parse_args( (array) $args, array(
    'orderby' => 'rand',
    'ignore_sticky_posts' => true,
    'return'  => 'query', // Valid values are: 'query' (WP_Query object), 'array' (the arguments array)
  ) );

  $related_args = array(
    'post_type'      => 'post', //get_post_type( $post_id ),
    'ignore_sticky_posts' => true,
    'posts_per_page' => $related_count,
    'post_status'    => 'publish',
    'post__not_in'   => array( $post_id ),
    'orderby'        => $args['orderby'],
    'tax_query'      => array()
  );

  $post       = get_post( $post_id );
  $taxonomies = get_object_taxonomies( $post, 'names' );

  foreach ( $taxonomies as $taxonomy ) {
    $terms = get_the_terms( $post_id, $taxonomy );
    if ( empty( $terms ) ) {
      continue;
    }
    $term_list                   = wp_list_pluck( $terms, 'slug' );
    $related_args['tax_query'][] = array(
      'taxonomy' => $taxonomy,
      'field'    => 'slug',
      'terms'    => $term_list
    );
  }

  if ( count( $related_args['tax_query'] ) > 1 ) {
    $related_args['tax_query']['relation'] = 'OR';
  }

  if ( $args['return'] == 'query' ) {
    return new WP_Query( $related_args );
  } else {
    return $related_args;
  }
}
