<?php

include_once get_template_directory() .  '/inc/rffw_scss/scssphp/scss.inc.php';
include_once get_template_directory() . '/inc/rffw_scss/class-rffw-scss.php';

$rfscss_settings = array(
  'scss_dir'  =>  get_template_directory() . '/sass/',
  'css_dir'   =>  get_template_directory() . '/css/',
  /*'compiling' =>  'Leafo\ScssPhp\Formatter\Crunched',
  'Leafo\ScssPhp\Formatter\Expanded'   => 'Expanded',
  'Leafo\ScssPhp\Formatter\Nested'     => 'Nested',
  'Leafo\ScssPhp\Formatter\Compressed' => 'Compressed',
  'Leafo\ScssPhp\Formatter\Compact'    => 'Compact',
  'Leafo\ScssPhp\Formatter\Crunched'   => 'Crunched',
  'Leafo\ScssPhp\Formatter\Debug'      => 'Debug'
  */
  'errors'    =>  'show-logged-in', //'show' 'hide'
);


$rfscss_compiler = new rffw_scss(
  $rfscss_settings['scss_dir'],
  $rfscss_settings['css_dir']
); 


function rffw_acf_save_post($post_id) {

  

	if($post_id == 'options'){
  	//$screen = get_current_screen(); var_dump($screen->base); // returns: '[parent-slug]_page_[your-menu-slug]'

    // Get all options Vars
    $variables = apply_filters('rffw_scss_variables', get_field_objects('options'));
    

    rffw_save_variables_to_sass($variables);
    rffw_scss_compile();
	}
}
add_action('acf/save_post', 'rffw_acf_save_post', 20);


// rffw_scss_needs_compiling() needs to be run as wp_head-action to make it possible
// for themes to set variables and decide if the style needs compiling
function rffw_scss_needs_compiling() {
  global $rfscss_compiler;

  $needs_compiling = apply_filters('rffw_scss_needs_compiling', $rfscss_compiler->needs_compiling());

  if ( $needs_compiling) {
      // Get all options Vars
      rffw_scss_compile();
      rfscss_handle_errors();
  }
}
add_action('wp_head', 'rffw_scss_needs_compiling');

function rffw_scss_compile($variables = array()) {
  global $rfscss_compiler;

  foreach ($variables as $variable_key => $variable_value) {
    if (strlen(trim($variable_value)) == 0) {
      unset($variables[$variable_key]);
    }
  }

  //$rfscss_compiler->set_variables($variables);
  $rfscss_compiler->compile();


}

function rffw_save_variables_to_sass($variables) {
  global $wp_filesystem;

  foreach ($variables as $variable_key => $variable_value) {
    if (strlen(trim($variable_value)) == 0) {
      unset($variables[$variable_key]);
    }
  }

  $scss_file_content = '';

  if(is_array($variables)){
    foreach ($variables as $key => $value) {
      // Write Lines of sass
      $scss_file_content .= '$'.$key.': '.$value.';'."\n";
    }
  }

  $wp_filesystem->put_contents(get_template_directory() . '/sass/theme/_theme_options.scss', $scss_file_content, FS_CHMOD_FILE);
}


function rfscss_settings_show_errors($errors) {
  echo
  '<style>
    .scss_errors {
      position: absolute;
      top: 0px;
      z-index: 99999;
      width: 100%;
      padding: 100px;
    }
    .scss_errors pre {
      background: #f5f5f5;
      border-left: 5px solid #DD3D36;
      box-shadow: 0 2px 3px rgba(51,51,51, .4);
      color: #666;
      font-family: monospace;
      font-size: 14px;
      margin: 20px 0;
      overflow: auto;
      padding: 20px;
      white-space: pre;
      white-space: pre-wrap;
      word-wrap: break-word;
    }
  </style>';

  echo '<div class="scss_errors"><pre>';
  echo '<h5 style="margin: 15px 0;"><strong>Only Admins see this error. Try Saving the theme settings to fix this error.</strong></h5>';

  foreach( $errors as $error) {
    echo '<p class="sass_error">';
    echo '<p>Sass Compiling Error</p>';
    echo '<strong>'. $error['file'] .'</strong> <br/><em>"'. $error['message'] .'"</em>';
    echo '<p>';
  }

  echo '</pre></div>';


}

function rfscss_handle_errors() {
    global $rfscss_settings, $rfscss_compiler;
    // Show to logged in users: All the methods for checking user login are set up later in the WP flow, so this only checks that there is a cookie
    if ( !is_admin() && $rfscss_settings['errors'] === 'show-logged-in' && !empty($_COOKIE[LOGGED_IN_COOKIE]) && count($rfscss_compiler->compile_errors) > 0) {
        rfscss_settings_show_errors($rfscss_compiler->compile_errors);
// Show in the header to anyone
    } else if ( !is_admin() && $rfscss_settings['errors'] === 'show' && count($rfscss_compiler->compile_errors) > 0) {
        rfscss_settings_show_errors($rfscss_compiler->compile_errors);
    } else {
      // Hide noting
    }
}

function rffw_scss_set_variables($theme_variable_objects){

    $theme_variables = array();

    $allowed_types_in_sass = array(
      'Fonts', 
      'true_false' , 
      'color_picker', 
      'extended-color-picker', 
      'number',
    );

    foreach ($theme_variable_objects as $key => $variable_object) {
      
      // Check if type is allowed add to sass
      if(in_array($variable_object['type'], $allowed_types_in_sass)){

        // fix sass vars start with enable to true or false
        if($variable_object['type'] == 'true_false'){
          if($variable_object['value']){
            $theme_variables[$key] = 'true';
          }
          else{
            $theme_variables[$key] = 'false';
          }
        }
        else{
            $theme_variables[$key] = $variable_object['value'];
        }
      }

      // Check if class sass is added
      if (strpos($variable_object['wrapper']['class'], 'sass') !== false) {
        $theme_variables[$key] = $variable_object['value'];
      }

    }

    // Flaten array with fuction
    $theme_variables = rffw_flatten_array($theme_variables);

    // Filter emtpy elements
    $theme_variables = array_filter($theme_variables, function($value) { return $value !== ''; });

    return $theme_variables;
}
add_filter('rffw_scss_variables','rffw_scss_set_variables');

// Rebuild array flatten to work with scss
function rffw_flatten_array($array, $prefix = '') {
  $result = array();
  foreach($array as $key=>$value) {
      if(is_array($value)) {
          $result = $result + rffw_flatten_array($value, $prefix . $key . '_');
      }
      else {
          $result[$prefix . $key] = $value;
      }
  }
  return $result;
}
