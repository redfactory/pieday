<?php
function register_acf_block_types() {

    acf_register_block_type(array(
        'name'              => 'testimonial',
        'title'             => __('Testimonial (in dev)'),
        'description'       => __('A custom testimonial block.'),
        'render_template'   => 'template-parts/block/testimonial/testimonial.php',
        'category'          => 'pieday-category',
        /*
        'icon'              => array(
                                'src' => 'admin-comments',
                                'foreground' => '#200597'
                              ),
                              */
        'icon'              => 'hammer',
        'keywords'          => array( 'testimonial', 'quote' ),
    ));

    acf_register_block_type(array(
        'name'              => 'slider',
        'title'             => __('Slider (in dev)'),
        'description'       => __('A custom slider block.'),
        'render_template'   => 'template-parts/block/slider/slider.php',
        'category'          => 'pieday-category',
        'mode'              => 'preview',
        'align'             => 'full',
        /*
        'icon'              => array(
                                'src' => 'images-alt2',
                                'foreground' => '#200597'
                              ),
                              */
        'icon'              => 'hammer',
        'keywords'          => array( 'slider'),
        'supports'          => array (
          'align' => false,
          'mode'  => false,
        ),
        //enqueue assets for slider
        'enqueue_assets' => function(){
          //wp_enqueue_style( 'block-testimonial', get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.css' );
          //wp_enqueue_script( 'block-testimonial', get_template_directory_uri() . '/template-parts/blocks/testimonial/testimonial.js', array('jquery'), '', true );
        },
    ));


    acf_register_block_type(array(
        'name'              => 'post-grid',
        'title'             => __('Post grid  (in dev)'),
        'description'       => __('Post grid block'),
        'render_template'   => 'template-parts/block/post-grid/post-grid.php',
        'category'          => 'pieday-category',
        'mode'              => 'preview',
        'align'             => 'full',
        /*
        'icon'              => array(
                                'src' => 'grid-view',
                                'foreground' => '#200597'
                              ),
                              */
        'icon'              => 'hammer',
        'keywords'          => array( 'grid', 'post'),
        'supports'          => array (
          'align' => false,
          'mode'  => false,
        ),
    ));

    acf_register_block_type(array(
        'name'              => 'menu-header',
        'title'             => __('Menu header (in dev)'),
        'description'       => __('Page header with menu button'),
        'render_template'   => 'template-parts/block/menu-header/menu-header.php',
        'category'          => 'pieday-category',
        'mode'              => 'preview',
        'align'             => 'full',
        /*
        'icon'              => array(
                                'src' => 'welcome-widgets-menus',
                                'foreground' => '#200597'
                              ),
        */
        'icon'              => 'hammer',
        'keywords'          => array( 'menu', 'header'),
        'supports'          => array (
          'align' => false,
          'mode'  => false,
        ),
    ));
}

// Check if function exists and hook into setup.
if( function_exists('acf_register_block_type') ) {
    add_action('acf/init', 'register_acf_block_types');
}

//Extra block category
function pieday_block_categories( $categories, $post ) {
    return array_merge(
        $categories,
        array(
            array(
                'slug' => 'pieday-category',
                'title' => __( 'Pieday', 'pieday' ),
            ),
        )
    );
}
add_filter( 'block_categories', 'pieday_block_categories', 10, 2 );


function pieday_acf_admin_head() {
  ?>
  <style type="text/css">

    .hndle.ui-sortable-handle{
      background:#f8f8f8;
    }

    #submitdiv .hndle.ui-sortable-handle{
      background:transparent;
    }

    #poststuff h2 {
        padding:8px 20px;
    }

    .ui-sortable .acf-fields > .acf-field{
      padding: 20px;
    }

    .acf-field-Fonts > .acf-label > label {
      font-size:20px;
    }

    .acf-field-Fonts > .acf-input {
      overflow:hidden;
      display:flex;
      flex-direction: row;
      flex-wrap: wrap;
      margin:0 -10px;
    }

    .acf-field-Fonts > .acf-input .acf-field {
      padding:10px;
      width:33%;
      flex: 0 0 33%;
    }

  </style>
  <?php
}

add_action('acf/input/admin_head', 'pieday_acf_admin_head');
