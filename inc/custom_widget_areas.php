<?php

$labels = array(
    'name'                  => _x( 'Widget Areas', 'Widget Area General Name', 'edgerblocks' ),
    'singular_name'         => _x( 'Widget Area', 'Widget Area Singular Name', 'edgerblocks' ),
    'menu_name'             => __( 'Widget Areas', 'edgerblocks' ),
    'name_admin_bar'        => __( 'Widget Area', 'edgerblocks' ),
    'parent_item_colon'     => __( 'Parent Item:', 'edgerblocks' ),
    'all_items'             => __( 'All Widget Areas', 'edgerblocks' ),
    'add_new_item'          => __( 'Add New Widget Area', 'edgerblocks' ),
    'add_new'               => __( 'Add New', 'edgerblocks' ),
    'new_item'              => __( 'New Widget Area', 'edgerblocks' ),
    'edit_item'             => __( 'Edit Widget Area', 'edgerblocks' ),
    'update_item'           => __( 'Update Widget Area', 'edgerblocks' ),
    'view_item'             => __( 'View Widget Area', 'edgerblocks' ),
    'view_items'            => __( 'View Widget Areas', 'edgerblocks' ),
        'search_items'        => __( 'Search Widgets', 'edgerblocks' ),
  'not_found'           => sprintf( __( 'Whoops, looks like you haven\'t created any widget areas yet. <a href="%s">Create one now</a>!', 'edgerblocks' ), admin_url( 'post-new.php?post_type=widget_area' ) ),
  'not_found_in_trash'  => __( 'No items found in Trash.', 'edgerblocks' ),
  );
  $args = array(
    'label'                 => __( 'Widget Area', 'edgerblocks' ),
    'description'           => __( 'Add a widget area to your theme to use on pages', 'edgerblocks' ),
    'labels'                => $labels,
    'supports'              => array( 'title', 'excerpt'  ),
    'hierarchical'          => false,
    'public'                => false,
    'show_ui'               => true,
    'show_in_menu'          => false,
    'show_in_admin_bar'     => false,
    'show_in_nav_menus'     => false,
    'can_export'            => true,
    'has_archive'           => true,
    'exclude_from_search'   => true,
    'publicly_queryable'    => false,
    'capability_type'       => 'page',
  );
  register_post_type( 'widget_area', $args );
  
function rffw_areas_register_sidebars(){
    $widgets = get_posts( 'post_type=widget_area&post_status=publish&order=ASC&posts_per_page=999&fields=ids' );

    if( !empty( $widgets ) && is_array( $widgets ) ){
            foreach ( $widgets as $widget ) {
                    $meta = get_post_meta( $widget );
                    $post_widget = get_post($widget);

                    register_sidebar( array(
                            'name'          =>  __( 'Widget Area ' . $post_widget->post_title, 'edgerblocks' ),
                            'id'            => 'widget-areas-' . $widget,
                            'description'   => $post_widget->post_excerpt,
                            'before_widget' => '<aside id="%1$s" class="widget %2$s rffw-col-replace-class">',
                            'after_widget'  => '</aside>',
                            'before_title'  => '<h3 class="widget-title">',
                            'after_title'   => '</h3>',
                    ) );
            }
    }
}
add_action( 'widgets_init', 'rffw_areas_register_sidebars' );

function rffw_areas_options_page() {
add_submenu_page(
    'themes.php',
                __( 'Widget Areas', 'edgerblocks' ),
                __( 'Widget Areas', 'edgerblocks' ),
                'manage_options',
                'edit.php?post_type=widget_area'
);
}
add_action( 'admin_menu', 'rffw_areas_options_page', 10 );