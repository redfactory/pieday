<?php

if( ! class_exists('rffw_acf_theme_options') ) :

class rffw_acf_theme_options extends acf_field {
	
	
	function __construct() {
		
		$this->name 	= 'theme-options';
		$this->label 	= __("Advanced Theme Options",'pieday');
		$this->category = 'layout';
		
		// do not delete!
		parent::__construct();
	}	
	
	function input_admin_enqueue_scripts() {
		
		// scripts
		wp_enqueue_script('rffw-acf-custom', get_template_directory_uri() . '/inc/js/backend-theme-options.js', array('jquery'), '', true);
		
		wp_register_script('acf-import-js',  get_template_directory_uri() . '/inc/acf-fields/js/acf-import.js', array('jquery'), '', true);
        wp_enqueue_script( 'acf-import-js' );
        wp_localize_script( 'acf-import-js', 'localizedData', array('import_nonce'  => wp_create_nonce( 'ajax_file_import_nonce' ) ));

	}	

	function render_field( $field ) {

		// vars
		// $text = acf_get_sub_array( $field, array('id', 'class', 'name', 'value') );
		 $hidden = acf_get_sub_array( $field, array('name', 'class', 'value') );

		// $palettes = apply_filters( "acf/rgba_color_picker/palette", true );

		// if ( $palettes == false ) {
		// 	$palettes = 'no-palette';
		// } else if ( !is_array($palettes) ) {
		// 	if ( $field['hide_palette'] == 1 ) {
		// 		$palettes = 'no-palette';
		// 	} else {
		// 		$palettes = $field['color_palette'];
		// 	}
		// } else {
		// 	if ( $field['hide_palette'] == 1 ) {
		// 		$palettes = 'no-palette';
		// 	} else {
		// 		$palettes = implode(";", $palettes);
		// 	}
		// }

		// $text['class'] = 'valuetarget';
		// $hidden['class'] = 'hiddentarget';
		
		// render
		$url = admin_url('admin.php?page=')."acf-options-advanced";
		
		?>
		<div class="acf-theme-options" data-target="target">
			<?php acf_hidden_input($hidden); ?>

								
				<h3><?php esc_html_e( 'Import options', 'pieday' ); ?></h3>

				<input id="file_import" type="file" name="file_import" required />
				<button class="button button-large" id="btn_import" type="button" ><?php _e( 'Upload file and import', 'pieday' ); ?></button>
				
				<?php if ( isset( $_GET['imported'] ) && $_GET['imported'] == 1 ): ?>
					<p><?php esc_html_e( 'Options successfully imported.', 'pieday' ); ?></p>
				<?php endif ?>
				<?php if ( isset( $_GET['imported'] ) && $_GET['imported'] == 2 ): ?>
					<p><?php esc_html_e( 'Some error happened during the upload process.', 'pieday' ); ?></p>
				<?php endif ?>

				<p class="acfim-info"><?php esc_attr_e( 'Your current options will be overwritten.', 'pieday' ); ?></p>
			
		
			<h3><?php esc_html_e( 'Export options', 'pieday' ); ?></h3>
			<a href="<?php echo $url.'&export';?>" class="button button-primary button-large"><?php _e('Download Export settings', 'pieday'); ?><a/>
			
			<h3><?php esc_html_e( 'Reset options', 'pieday' ); ?></h3>
			<a href="<?php echo $url.'&reset';?>" class="button button-large"><?php _e('Reset settings', 'pieday'); ?><a/>
		</div>

		<?php
	}	

	
	function render_field_settings( $field ) {
		
		// // default value
		// acf_render_field_setting( $field, array(
		// 	'label'			=> __('Default Value','pieday'),
		// 	'instructions'	=> '',
		// 	'type'			=> 'text',
		// 	'name'			=> 'default_value',
		// 	'placeholder'	=> '#FFFFFF'
		// ));
		
		// // color palette
		// acf_render_field_setting( $field, array(
		// 	'label'			=> __('Color Palette','pieday'),
		// 	'instructions'	=> __('Enter color codes separated by semicolons. You can use HEX or RGBA color codes and can also mix them (e.g. #2ecc71; rgba(50,40,30,0.5).<br><br>This can (and is maybe) overwritten by the "acf/acfrb_color_picker/palette" filter.','acf-extended-color-picker'),
		// 	'type'			=> 'text',
		// 	'name'			=> 'color_palette'
		// ));
		
		// // hide palette
		// acf_render_field_setting( $field, array(
		// 	'label'			=> __('Hide Color Palette','pieday'),
		// 	'instructions'	=> __('Don\'t show a color palette in the color picker', 'pieday'),
		// 	'type'			=> 'true_false',
		// 	'name'			=> 'hide_palette',
		// 	'ui'			=> 1,
		// ));
	}
	
}

// initialize
acf_register_field_type( new rffw_acf_theme_options() );

endif;



add_action( 'admin_init', 'rffw_import_export_reset_options' );

function rffw_import_export_reset_options() {
	// $current_screen = get_current_screen();
	$url = admin_url('admin.php?page=')."acf-options-advanced";
	

	if ( isset( $_GET['import'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'acf-options-advanced' ) {

		if ( ! empty( $_FILES['backup'] ) ) {
			$target_dir  = $this->plugin_path . '/temp/';
			$target_file = $target_dir . basename( $_FILES['backup']['name'] );
			if ( move_uploaded_file( $_FILES['backup']['tmp_name'], $target_file ) ) {
				WP_Filesystem();
				
				// $unzip = unzip_file( $target_file, $target_dir );
				// unlink( $target_file );

				// if ( is_wp_error( $unzip ) || ! file_exists( $this->plugin_path . '/temp/images.json' ) || ! file_exists( $this->plugin_path . '/temp/options.json' ) ) {
				// 	wp_redirect( $plugin_url . '&imported=2' );
				// 	die();
				// }

				$json 	  = file_get_contents( $this->plugin_path . '/temp/options.json' );
				$options  = json_decode( $json, true );
				// $img_json = file_get_contents( $this->plugin_path . '/temp/images.json' );
				// $img_data = array();

				// if ( ! empty( $img_json ) ) {
				// 	$img_data = json_decode( $img_json, true );
				// }

				// if ( ! empty( $img_data ) ) {
				// 	$upload_dir = wp_upload_dir();
				// 	require_once ABSPATH . 'wp-admin/includes/image.php';
				// 	foreach ( $img_data as $key => $image ) {

				// 		$image_url  = $this->plugin_path . '/temp/' . $image['name'];
				// 		$image_data = file_get_contents( $image_url );
				// 		$filename   = basename( $image_url );

				// 		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
				// 			$file = $upload_dir['path'] . '/' . $filename;
				// 		} else {
				// 			$file = $upload_dir['basedir'] . '/' . $filename;
				// 		}

				// 		file_put_contents( $file, $image_data );

				// 		$wp_filetype = wp_check_filetype( $filename, null );

				// 		$attachment = array(
				// 			'post_mime_type' => $wp_filetype['type'],
				// 			'post_title' 	 => sanitize_file_name( $filename ),
				// 			'post_content' 	 => '',
				// 			'post_status' 	 => 'inherit'
				// 		);

				// 		$attach_id = wp_insert_attachment( $attachment, $file );
				// 		$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
				// 		wp_update_attachment_metadata( $attach_id, $attach_data );

				// 		$img_data[ $key ]['new_id'] = $attach_id;
				// 	}
				// }
				
				foreach ( $options as $key => $option ) {
					// if ( ( $option['type'] == 'image' || $option['type'] == 'gallery' ) && ! empty( $img_data ) ) {
					// 	if ( $option['type'] == 'image' ) {
					// 		$options[ $key ]['value'] = $img_data[ $option['value'] ]['new_id'];
					// 	}
						
					// 	if ( $option['type'] == 'gallery' ) {
					// 		foreach ( $options[ $key ]['value'] as $key2 => $value) {
					// 			$options[ $key ]['value'][ $key2 ] = $img_data[ $options[ $key ]['value'][ $key2 ] ]['new_id'];
					// 		}
					// 	}
					// }

					update_option( '_options_' . $key, $options[ $key ]['key'], 'no' );
					update_option( 'options_' . $key, $options[ $key ]['value'], 'no' );
				}

				$files = glob( $this->plugin_path . '/temp/*' );
				foreach( $files as $file ) {
					if( is_file( $file ) ){
						unlink( $file );
					}
				}
			}
		}
		// unzip_file()
		//wp_redirect( $plugin_url . '&imported=1' );
		die();
	}

	if ( isset( $_GET['export'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'acf-options-advanced' ) {	
		$opts = array();
		$imgs = array();

		global $wpdb;
		$keys = $wpdb->get_col($wpdb->prepare(
			"SELECT option_value FROM $wpdb->options WHERE option_name LIKE %s",
			'_options_%' 
		));


		foreach ($keys as $key => $option) {
			$opt_data = get_field_object($option, 'options');

			if ( is_array( $opt_data ) ) {
				$option_value = get_option( 'options_' . $opt_data['_name'] );
				$opts[ $opt_data['_name'] ] = array(
					'key' => $opt_data['key'],
					'type' => $opt_data['type'],
					'value' => $option_value
				); 

				// if ( ! empty( $option_value ) ) {
				// 	if ( $opt_data['type'] == 'image' ) {
				// 		$imgs[ $option_value ] = array('old_id' => $option_value );
				// 	}
				// 	if ( $opt_data['type'] == 'gallery' && is_array( $option_value ) ) {
				// 		foreach ( $option_value as $img ) {
				// 			$imgs[ $img ] = array('old_id' => $img );
				// 		}
				// 	}
				// }
			}
		}


		// if ( ! empty( $imgs ) ) {
		// 	foreach ($imgs as $key => $img) {
		// 		$image_data = wp_get_attachment_image_src( $key, 'full' );
		// 		$parts  	= explode('uploads', $image_data[0] );
		// 		$parts2 	= explode('/', $image_data[0] );

		// 		copy ( WP_CONTENT_DIR . '/uploads' . $parts[1], $this->plugin_path . '/temp/' . end( $parts2 ) );
		// 		$imgs[ $key ]['name'] = end( $parts2 );
		// 	}
		// 	$json = json_encode( $imgs );
		// 	file_put_contents( $this->plugin_path . '/temp/images.json', $json );
		// }
		
		$json = json_encode( $opts, JSON_PRETTY_PRINT);
		header('Content-disposition: attachment; filename=options.json');
		header('Content-type: application/json');
		echo $json;
		
		//wp_redirect( $url.'&exported=1' );
		die();
	}


	if ( isset( $_GET['reset'] ) && isset( $_GET['page'] ) && $_GET['page'] == 'acf-options-advanced' ) {	

		global $wpdb;

		$keys = $wpdb->get_col($wpdb->prepare(
			"SELECT option_value FROM $wpdb->options WHERE option_name LIKE %s",
			'_options_%' 
		));

		foreach ($keys as $key => $option) {
			$opt_data = get_field_object($option, 'options');

			if ( is_array( $opt_data ) ) {
				$option_value = delete_option('options_' . $opt_data['_name'] );
			}
		}

		
		wp_redirect( $url.'&rested=1' );
		die();
	}
}

add_action( 'wp_ajax_ajax_file_import', 'ajax_import_example'  );
function ajax_import_example() {
	check_ajax_referer( 'ajax_file_import_nonce' );

	$raw_content = array();
	$i = 0;
	while ( isset( $_FILES[ 'file_' . $i ] ) ) {
		$file_arr = $_FILES[ 'file_' . $i ];
		$file_content = file_get_contents( $file_arr['tmp_name'] );
		$raw_content[]  = json_decode( $file_content, true );
		$i++;
	}

	$options = $raw_content[0];

	
	foreach ( $options as $key => $option ) {
		update_option( '_options_' . $key, $options[ $key ]['key'], 'no' );
		update_option( 'options_' . $key, $options[ $key ]['value'], 'no' );
	}


	// (OPTIONAL) Add data to return to AJAX Success
	$rtn_data = array(
		'action'               => 'apl_import',
		'_ajax_nonce'          => wp_create_nonce( 'alt_ajax' ),
	);

	die();
}