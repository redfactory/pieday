jQuery(document).ready(function($){
    // PHP files could echo an Nonce. This however localizes scripts.
    var importNonce = localizedData.import_nonce;

    $('#btn_import').click( function( event ) {
        event.stopPropagation(); // Stop stuff happening
        event.preventDefault(); // Totally stop stuff happening

        var formData = new FormData();
        var importFiles = $('#file_import')[0].files;

        // For EACH file, append to formData.
        // NOTE: Just appending all of importFiles doesn't transition well to PHP.
        jQuery.each( importFiles, function( index, value ) {
            var name = 'file_' + index;
            formData.append( name, value )
        });

        formData.append( 'action', 'ajax_file_import' );
        formData.append( '_ajax_nonce', importNonce );

        jQuery.ajax({
            url: ajaxurl,
            type: 'POST',
            data: formData,
            cache: false,
            dataType: 'json', // This replaces dataFilter: function() && JSON.parse( data ).
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request


            beforeSend: function( jqXHR, settings ){

            },
            success: function( data, textStatus, jqXHR ) {
                console.log( 'Return from PHP AJAX function/method.' );


            },
            complete: function(jqXHR, textStatus){
                console.log( 'AJAX is complete.' );
            }
        });// End AJAX.
    });// End .submit().
});