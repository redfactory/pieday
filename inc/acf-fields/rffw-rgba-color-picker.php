<?php

if( ! class_exists('rffw_acf_field_extended_color_picker') ) :

class rffw_acf_field_extended_color_picker extends acf_field {
	
	function __construct() {
		
		$this->name = 'extended-color-picker';
		$this->label = __("RGBA Color Picker",'acf-extended-color-picker');
		$this->category = 'jquery';
		$this->defaults = array(
			'hide_palette'	=> '',
			'color_palette'	=> '',
		);
		
		// do not delete!
		parent::__construct();
		
	}	
	
	function input_admin_enqueue_scripts() {
		
		// Get the theme data.
		$the_theme     = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );

		$css_version = $theme_version . '.' . filemtime( get_template_directory() . '/css/theme.min.css' );
		$js_version = $theme_version . '.' . filemtime( get_template_directory() . '/js/theme.min.js' );

		// globals
		global $wp_scripts, $wp_styles;		

		// Add the Alpha Color Picker JS
		wp_enqueue_script( 'wp-color-picker-alpha', get_template_directory_uri() ."/inc/acf-fields/js/wp-color-picker-alpha.js", array( 'wp-color-picker' ), $js_version, true );

		// // register Extended Color Picker JS
		 wp_register_script( 'acf-rgba-color-picker-script', get_template_directory_uri() ."/inc/acf-fields/js/acf-rgba-color-picker.js", array('wp-color-picker-alpha'), $js_version, true );
		
		// enqueue styles & scripts
		wp_enqueue_script('wp-color-picker');
		wp_enqueue_script('acf-rgba-color-picker-script');

				
	}	

	function render_field( $field ) {

		// vars
		$text = acf_get_sub_array( $field, array('id', 'class', 'name', 'value') );
		$hidden = acf_get_sub_array( $field, array('name', 'class', 'value') );

		$palettes = apply_filters( "acf/rgba_color_picker/palette", true );

		if ( $palettes == false ) {
			$palettes = 'no-palette';
		} else if ( !is_array($palettes) ) {
			if ( $field['hide_palette'] == 1 ) {
				$palettes = 'no-palette';
			} else {
				$palettes = $field['color_palette'];
			}
		} else {
			if ( $field['hide_palette'] == 1 ) {
				$palettes = 'no-palette';
			} else {
				$palettes = implode(";", $palettes);
			}
		}

		$text['class'] = 'valuetarget';
		$hidden['class'] = 'hiddentarget';
		
		// render
		?>
		<div class="acf-color-picker" data-target="target" data-palette='<?php echo $palettes ?>' data-default="<?php echo $field['default_value'] ?>">			
			<?php acf_hidden_input($hidden); ?>
			<input type="text" <?php echo acf_esc_attr($text); ?> data-alpha ="true" />
		</div>
		<?php
	}	

	
	function render_field_settings( $field ) {
		
		// default value
		acf_render_field_setting( $field, array(
			'label'			=> __('Default Value','acf'),
			'instructions'	=> '',
			'type'			=> 'text',
			'name'			=> 'default_value',
			'placeholder'	=> '#FFFFFF'
		));
		
		// color palette
		acf_render_field_setting( $field, array(
			'label'			=> __('Color Palette','acf-extended-color-picker'),
			'instructions'	=> __('Enter color codes separated by semicolons. You can use HEX or RGBA color codes and can also mix them (e.g. #2ecc71; rgba(50,40,30,0.5).<br><br>This can (and is maybe) overwritten by the "acf/acfrb_color_picker/palette" filter.','acf-extended-color-picker'),
			'type'			=> 'text',
			'name'			=> 'color_palette'
		));
		
		// hide palette
		acf_render_field_setting( $field, array(
			'label'			=> __('Hide Color Palette','acf-extended-color-picker'),
			'instructions'	=> __('Don\'t show a color palette in the color picker','acf-extended-color-picker'),
			'type'			=> 'true_false',
			'name'			=> 'hide_palette',
			'ui'			=> 1,
		));
	}
	
}

// initialize
acf_register_field_type( new rffw_acf_field_extended_color_picker() );

endif;
