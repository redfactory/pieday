<?php
// Get default values from json files
function pieday_get_default_from_json_files(){

	// Load from global if function has run before
	if(isset($GLOBALS['rrfw_acf_defaults'])) return $GLOBALS['rrfw_acf_defaults'];

	// Get Json files from folder
	$files = glob(get_template_directory().'/inc/acf-json/*.{json}', GLOB_BRACE);
	
	// Start with empty array
	$rrfw_acf_defaults = array();

	// Loop files with options in folder
	foreach($files as $file) {
		// Open file en decode json
		$json = json_decode(file_get_contents($file), true);

		// Loop option fields in file
		foreach($json['fields'] as $field) {

			// Skip options without default value
			if(isset($field['default_value'])){

				// if default value is array return first element of array
				if(is_array($field['default_value'])){
					if(isset($field['default_value'][0])){
						$rrfw_acf_defaults[$field['name']] = $field['default_value'][0];
					}
					else{
						$rrfw_acf_defaults[$field['name']] = '';
					}
				}else{
					$rrfw_acf_defaults[$field['name']] = $field['default_value'];
				}

			}
		}
	}

	// Save to global to prevent running function again
	$GLOBALS['rrfw_acf_defaults'] = $rrfw_acf_defaults;

	return $rrfw_acf_defaults;
}

// Check if acf is active if not adds own get_field function to fake acf and load default values
function the_acf_check(){

	if( !is_admin() && !function_exists('get_field')) {

		function get_field( $key, $post_id = false, $format_value = true ) {
			if( false === $post_id ) {
				global $post;
				$post_id = $post->ID;
			}
			
           // $value = get_post_meta( $post_id, $key, true );
            $value = apply_filters('acf/load_value', NULL, 0, $key);
			return $value;
		}
	}
}
add_action( 'after_setup_theme', 'the_acf_check' );

// the acf filter to load default values from the array above
function rffw_acf_load_value( $value, $post_id= 0, $key=''){
	$rrfw_acf_defaults = pieday_get_default_from_json_files();

    if(is_array($key)){
        $key = $key['name'];
	}

	// If NULL not set use default
	if($value === NULL && isset($rrfw_acf_defaults[$key])){
		$value = $rrfw_acf_defaults[$key];
	}
	return $value;
	
}
add_filter('acf/load_value', 'rffw_acf_load_value', 10, 3);
	
 
  // Get get field with fallback to post en default array
  function rffw_get_field($key){
	  global $post;
	  
  
	  if( function_exists('get_field')){
		  if(is_home()){
			  if(!empty(get_option( 'page_for_posts' ))){
				$value = get_field( $key.'_post', get_option( 'page_for_posts' ));
			  }
		  }
		  elseif(rffw_is_index()){

		  }
		  elseif(isset($post->ID)){
			  $value = get_field( $key.'_post', $post->ID );
		  }

		  if( !isset($value) || $value =='default' || $value === NULL ){
			  $value = get_field( $key, 'option' );
		  }
	  }
	  else{
		$rrfw_acf_defaults = pieday_get_default_from_json_files();

		if(isset($rrfw_acf_defaults[$key])){
			$value = $rrfw_acf_defaults[$key];
		}
	  }

	return $value;
  }

  function acf_load_sidebars( $field ) {
    
    // reset choices
    $field['choices'] = array(
		'default' => __('Default','pieday'),
	);
    
	$sidebars_widgets = wp_get_sidebars_widgets();

    // loop through array and add to field 'choices'
    if( is_array($sidebars_widgets) ) {
        
        foreach( $sidebars_widgets as $sidebars_widget_name => $sidebars_widget  ) {
            
            $field['choices'][ $sidebars_widget_name ] = $sidebars_widget_name;
            
        }
        
    }
    // return the field
    return $field;
    
}
add_filter('acf/load_field/name=right_sidebar', 'acf_load_sidebars');
add_filter('acf/load_field/name=left_sidebar', 'acf_load_sidebars');