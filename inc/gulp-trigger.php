<?php

function rffw_post_gulp_trigger( $post_id, $post, $update ) {
    global $wp_filesystem;

    $wp_filesystem->put_contents(get_template_directory().'/inc/gulp_trigger_file.php', time(), FS_CHMOD_FILE);
}

if(defined('RFFW_BS_TRIGGER') && RFFW_BS_TRIGGER ){
    add_action( 'save_post', 'rffw_post_gulp_trigger', 10, 3 );
}
