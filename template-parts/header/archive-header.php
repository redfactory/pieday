<?php
$bg_img_url = '';

if(is_home()){
    if(!empty(get_option( 'page_for_posts' ))){
			$bg_img_url = get_the_post_thumbnail_url(get_option( 'page_for_posts' ), 'full');
    }
}
else{
    if(get_the_post_thumbnail() ) {
      $bg_img_url = get_the_post_thumbnail_url($post->ID, 'full');
    }
}
?>

<header class="entry-header page-header <?php if(!empty($bg_img_url)) echo 'has-image'; ?>" style="background-image:url(<?php echo esc_url($bg_img_url); ?>);">
  
  <?php //if (!isset($bg_img_url) || $bg_img_url == '') : ?>
  <svg class="header-border" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 100" preserveAspectRatio="none">
    <path d="M 1 1 H 199 V 75 L 1 99 Z" vector-effect="non-scaling-stroke" />
  </svg>
  <?php //endif; ?>

  <div class="overlay">

    <?php if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );  ?>

    <h1 class="entry-title"><?php echo pieday_add_title_border(get_the_archive_title( '<h1 class="page-title">', '</h1>' ), 1); ?></h1>

    <div class="entry-meta">
    <?php the_archive_description( '<div class="taxonomy-description">', '</div>' ); ?>
    </div><!-- .entry-meta -->

  </div>

    <svg class="header-diagonal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon points="0,100 100,0 100,100"/>
    </svg>
</header>
