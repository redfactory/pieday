<?php
  if ( isset( $_GET['author_name'] ) ) {
    $curauth = get_user_by( 'slug', $author_name );
  } else {
    $curauth = get_userdata( intval( $author ) );
  }
?>

<header class="entry-header page-header author-header">

  <svg class="header-border" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 100" preserveAspectRatio="none">
    <path d="M 1 1 H 199 V 75 L 1 99 Z" vector-effect="non-scaling-stroke" />
  </svg>

  <div class="overlay">

    <?php  if ( function_exists('yoast_breadcrumb') ) yoast_breadcrumb( '<p id="breadcrumbs">','</p>' );  ?>
    
    <h1><?php echo pieday_add_title_border(esc_html__( 'About:', 'pieday' ) . ' ' . esc_html( $curauth->nickname ), 1); ?></h1>

    <?php if ( ! empty( $curauth->user_url ) || ! empty( $curauth->user_description ) ) : ?>
    <div class="entry-meta">
      <dl>
        <?php if ( ! empty( $curauth->user_url ) ) : ?>
          <dt><?php esc_html_e( 'Website', 'pieday' ); ?></dt>
          <dd>
            <a href="<?php echo esc_url( $curauth->user_url ); ?>"><?php echo esc_html( $curauth->user_url ); ?></a>
          </dd>
        <?php endif; ?>

        <?php if ( ! empty( $curauth->user_description ) ) : ?>
          <dt><?php esc_html_e( 'Profile', 'pieday' ); ?></dt>
          <dd><?php esc_html_e( $curauth->user_description ); ?></dd>
        <?php endif; ?>
      </dl>
    </div>
    <?php endif; ?>

  </div>
    <svg class="header-diagonal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon points="0,100 100,0 100,100"/>
    </svg>
</header>
