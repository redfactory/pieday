<?php
/**
 * The right sidebar containing the main widget area.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( pieday_select_sidebar('right-sidebar' , 'right_sidebar')  ) ) {
	return;
}

?>

<?php if ( 'both' === rffw_get_field('sidebar_position') ) : ?>
	<div class="col-md-3 widget-area" id="right-sidebar" role="complementary">
<?php else : ?>
	<div class="col-md-4 widget-area" id="right-sidebar" role="complementary">
<?php endif; ?>
<?php dynamic_sidebar( pieday_select_sidebar('right-sidebar' , 'right_sidebar') ); ?>

</div><!-- #right-sidebar -->
