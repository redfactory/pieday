<?php
/**
 * The sidebar containing the main widget area.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! is_active_sidebar( pieday_select_sidebar('left-sidebar' , 'left_sidebar')  ) ) {
	return;
}

// when both sidebars turned on reduce col size to 3 from 4.
$sidebar_pos = rffw_get_field('sidebar_position');
?>

<?php if ( 'both' === $sidebar_pos ) : ?>
	<div class="col-md-3 widget-area" id="left-sidebar" role="complementary">
<?php else : ?>
	<div class="col-md-4 widget-area" id="left-sidebar" role="complementary">
<?php endif; ?>
<?php dynamic_sidebar( pieday_select_sidebar('left-sidebar' , 'left_sidebar') ); ?>

</div><!-- #left-sidebar -->
