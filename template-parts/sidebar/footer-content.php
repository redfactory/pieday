<?php
/**
 * Sidebar setup for footer full.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = rffw_get_field('container_type');

?>

<?php if ( is_active_sidebar( 'footer-content' ) ) : ?>
	<div class="wrapper" id="wrapper-footer-content">
		<div class="<?php echo esc_attr( $container ); ?>" id="footer-full-content" tabindex="-1">
			<div class="row">
				<?php dynamic_sidebar( 'footer-content' ); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
