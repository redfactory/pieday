<?php
/**
 * Post rendering content according to caller of get_template_part.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

?>

<article <?php post_class('overview-item'); ?> id="post-<?php the_ID(); ?>">

	<header class="entry-header">

		<?php if(has_post_thumbnail()) : ?>
			<?php
			 	$feat_img = get_the_post_thumbnail_url( $post->ID, 'large' );
			?>
			<div class="featured-image" style="background-image:url(<?php echo $feat_img; ?>)"></div>
		<?php endif; ?>

		<?php
		the_title(
			sprintf( '<h2 class="entry-title"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ),
			'</a></h2>'
		);
		?>

		<?php if ( 'post' == get_post_type() ) : ?>

			<div class="entry-meta">
				<?php pieday_posted_on(); ?>
			</div><!-- .entry-meta -->

		<?php endif; ?>

	</header><!-- .entry-header -->

	<div class="entry-content">

		<p class="excerpt"><?php echo pieday_short_excerpt(); ?></p>
		<a href="<?php echo get_permalink(); ?>" class="btn read-more"><?php _e('Read more', 'pieday'); ?></a>

	</div><!-- .entry-content -->

	<!-- <footer class="entry-footer">

		<?php //pieday_entry_footer(); ?>

	</footer> -->

</article><!-- #post-## -->
