<?php
/**
 * Single post partial template.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

<article <?php post_class(); ?> id="post-<?php the_ID(); ?>">

	<div class="entry-content">
		<div class="entry-meta">
			<?php esc_html_e('Categories: ', 'pieday'); ?><?php the_category(', '); ?>
		</div>
		<?php the_content(); ?>

		<?php
		wp_link_pages(
			array(
				'before' => '<div class="page-links">' . __( 'Pages:', 'pieday' ),
				'after'  => '</div>',
			)
		);
		?>

	</div><!-- .entry-content -->

	<footer class="entry-footer">

		<?php pieday_entry_footer(); ?>

		<?php pieday_post_nav(); ?>

		<?php pieday_share_buttons(); ?>

	</footer><!-- .entry-footer -->

</article><!-- #post-## -->
