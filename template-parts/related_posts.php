<?php $related_posts = rffw_get_related_posts(get_the_ID(), 3);  ?>

<?php if(!empty($related_posts)) : ?>
  <section id="related-posts">
    <div class="container">
      <div class="related-posts-wrapper">
        <h3><?php _e('Related posts', 'pieday'); ?></h3>
        <div class="related-posts-grid">
          <?php	while ( $related_posts->have_posts() ) : $related_posts->the_post(); ?>
            <?php get_template_part( 'template-parts/loop/content', get_post_format() ); ?>
          <?php endwhile; wp_reset_postdata(); ?>
        </div>
      </div>
    </div>
  </section>
<?php endif; ?>
