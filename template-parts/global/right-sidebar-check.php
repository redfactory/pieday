<?php
/**
 * Right sidebar check.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>

</div><!-- #closing the primary container from /template-parts/global/left-sidebar-check.php -->

<?php $sidebar_pos = rffw_get_field('sidebar_position'); ?>

<?php if ( 'right' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>

	<?php get_template_part( 'template-parts/sidebar/sidebar', 'right' ); ?>

<?php endif; ?>
