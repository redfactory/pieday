<?php
/**
 * Left sidebar check.
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$sidebar_pos = rffw_get_field('sidebar_position');
?>

<?php if ( 'left' === $sidebar_pos || 'both' === $sidebar_pos ) : ?>
	<?php get_template_part( 'template-parts/sidebar/sidebar', 'left' ); ?>
<?php endif; ?>

<div class="col-md content-area" id="primary">
