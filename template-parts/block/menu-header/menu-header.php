<?php
/**
 * Menu Header Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */


// Create class attribute allowing for custom "className" and "align" values.
$className = 'menu-header-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

?>

<div id="<?php esc_attr_e('menu-header-' . $block['id']); ?>" class="alignwide <?php esc_attr_e($className); ?>">
  <header class="entry-header page-header menu-header" style="background-image:url(<?php echo esc_url(get_field('image')); ?>);">
    <div class="overlay">
      <h1 class="entry-title"><?php echo pieday_add_title_border(get_field('title'), get_field('underlined_words')); ?></h1>
    </div>
    <svg class="header-border" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 200 100" preserveAspectRatio="none">
      <path d="M 1 1 H 199 V 89 L 1 99 Z" vector-effect="non-scaling-stroke" />
    </svg>
    <svg class="header-diagonal" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 100 100" preserveAspectRatio="none">
      <polygon points="0,100 100,0 100,100"/>
    </svg>
  </header>
</div>
