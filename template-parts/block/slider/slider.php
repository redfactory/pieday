<?php
/**
 * Slider Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'slider-' . $block['id'];

// Create class attribute allowing for custom "className" and "align" values.
$className = 'slider-container-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

// Load values and assing defaults.
$show_arrows = get_field('show-arrows');
$show_pagination = get_field('show-pagination');

?>

<div  id="<?php echo $id; ?>" class=" <?php echo $className; ?>">
  <div class="swiper-container">
    <div class="swiper-wrapper">
      <?php if( have_rows('slide') ): ?>
        <?php while ( have_rows('slide') ) : the_row(); ?>
            <?php
              $bg_image = get_sub_field('image');
              $bg_image_url = $bg_image['url'];
            ?>
            <div class="swiper-slide" style="background-image:url(<?php echo $bg_image_url; ?>)">
              <div class="slide-content">
                <h3 class="slide-title">
                  <?php the_sub_field('title'); ?>
                </h3>
              </div>
            </div>
        <?php endwhile; ?>
      <?php endif; ?>
    </div>

  </div>

      <div class="swiper-pagination"></div>
      <?php if($show_arrows) : ?>
        <div class="swiper-btn swiper-button-prev"></div>
        <div class="swiper-btn swiper-button-next"></div>
      <?php endif; ?>
</div>
<script>
  jQuery(document).ready(function ($) {
    if (!$('body').hasClass("wp-admin")) {
      var BlockSwiper = new Swiper($('#<?php echo $id?> .swiper-container'), {
        mode: 'horizontal',
        loop: true,
        <?php if($show_arrows) : ?>
        navigation: {
          nextEl: '#<?php echo $id?> .swiper-button-next',
          prevEl: '#<?php echo $id?> .swiper-button-prev',
        },
        <?php endif; ?>
        <?php if($show_pagination) : ?>
        pagination: {
           el: '#<?php echo $id?> .swiper-pagination',
           clickable: true
        },
        <?php endif; ?>
        lazyLoading: true
      });
    }
  });
</script>
