<div class="col-6 col-sm-6 col-md-4">
  <a href="<?php the_permalink(); ?>" class="post-item">
    <div class="feat-img" style="background-image:url(<?php echo $feat_img_url; ?>)"></div>
    <h4><?php the_title(); ?></h4>
  </a>
</div>
