<?php
/**
 * Slider Block Template.
 *
 * @param   array $block The block settings and attributes.
 * @param   string $content The block inner HTML (empty).
 * @param   bool $is_preview True during AJAX preview.
 * @param   (int|string) $post_id The post ID this block is saved to.
 */

// Create id attribute allowing for custom "anchor" value.
$id = 'postgrid-' . $block['id'];

// Create class attribute allowing for custom "className" and "align" values.
$className = 'post-grid-block';
if( !empty($block['className']) ) {
    $className .= ' ' . $block['className'];
}

$show_posts = get_field('show_posts');
?>


<div id="<?php echo $id; ?>" class="<?php echo $className; ?>">
  <?php if(get_field('title')) : ?>
    <h3 class="block-title"><?php get_field('title') ?></h3>
  <?php endif; ?>

  <?php if($show_posts == 'show-selected') : ?>
    <?php if( have_rows('selected-posts') ): ?>
      <div class="row">
        <?php while ( have_rows('selected-posts') ) : the_row(); ?>
          <?php
            $spost = get_sub_field('selected-post');
            $feat_img_url = get_the_post_thumbnail_url($spost);
          ?>
          <?php include('post-grid-content.php')?>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>
  <?php else : ?>
    <?php
    $args = get_posts(array(
                'numberposts'	=> -1,
                'post_type'		=> 'post',
                'order'=>'DESC',
              ));
    $sposts_query = new WP_Query($args);
    ?>
    <?php if( $sposts_query->have_posts() ): ?>
      <div class="row">
        <?php while( $sposts_query->have_posts() ) : $sposts_query->the_post(); ?>
          <?php
            $feat_img_url = get_the_post_thumbnail_url();
          ?>
          <?php include('post-grid-content.php')?>
        <?php endwhile; ?>
      </div>
    <?php endif; ?>

  <?php endif; ?>
  <?php wp_reset_query(); ?>
</div>
