<?php
/**
 * PieDay functions and definitions
 *
 * @package pieday
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

define('RFFW_SHOW_AUTHORS', true ); // hide authors
define('RFFW_SCSS_ALWAYS_RECOMPILE', false); // force compile
define('RFFW_BS_TRIGGER', true); // force browsersync reload on save wordpress post

$pieday_includes = array(
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/tgma.php',    						// Load custom WordPress nav walker.
	'/class-tgm-plugin-activation.php',    	// Load plugins
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
	'/deprecated.php',                      // Load deprecated functions.
	'/acf-loader.php',                      // Load deprecated functions.
	'/gulp-trigger.php',                    // Gulp Browsersync trigger script
	'/acf-extra.php',                      	// Load deprecated functions.
	'/acf-blocks.php',						// loads functions folder
	'/cleanup-wp.php',						// loads functions folder
	'/custom_widget_areas.php',				// loads functions folder
	'/helpers.php',							// loads functions folder
	'/../blocks/index.php',					// loads all blocks
	'/rffw_scss/rffw_scss.php'				// load SCSS Compilcer functions
);

foreach ( $pieday_includes as $file ) {
	$filepath = locate_template( 'inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

